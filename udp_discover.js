/**
 * A little service to be run aside of the main app.
 * 
 * It tries to find a Core Manager instance on the LAN using
 * UDP Broadcast.
 * 
 * Inspired by https://gist.github.com/sid24rane/6e6698e93360f2694e310dd347a2e2eb
 */

import { createSocket } from "dgram";
import { createServer } from "http";
import { networkInterfaces } from "os";

const CoreUDPPort = 42012;
const selfHTTPPort = 8080;

const ifaces = networkInterfaces();
console.log(ifaces)

let candidates = [ "127.0.0.1" ];
for (let key in ifaces) {
    let v4s = ifaces[key].filter(e => e.family == "IPv4" && e.address != "127.0.0.1");
    candidates.push(...v4s.map(e => {
        let addr = e.address.split(".");
        addr[3] = "255";
        return addr.join('.');
    }));
}
console.log("Broadcasting on:", candidates);

let srvs = [];

const client = createSocket("udp4");
const server = createServer((req, res) => {
    
    switch (req.url) {

        case "/":
            res.statusCode = 200;
            res.write(JSON.stringify(srvs));
            break;

        case "/search":
            res.statusCode = 200;
            search();
            break;

        default:
            res.statusCode = 404;
            res.write("Po trouvé");
    }

    res.end();
});

function printSrv (srv, ip) {
    console.log(
        `\x1b[38;2;${srv.color.r};${srv.color.g};${srv.color.b}m`,
        srv.id, 
        "@ \x1b[31;47m", ip,
        `\x1b[0m\x1b[38;2;${srv.color.r};${srv.color.g};${srv.color.b}m`,
        "-", srv.description, 
        "\x1b[0m"
    );
}

function search () {
    const data = Buffer.from("Déclinez votre identité: Nom, Age, Profession de la daronne.");
    for (let nm of candidates) {
        client.send(data, CoreUDPPort, nm);
    }
}


process.on("SIGINT", () => {
    console.log("o7");
    client.close();
    server.close();
});

client.on("message", (msg, info) => {    
    
    let srvInfo;
    let txt = msg.toString();
    
    try {
        srvInfo = JSON.parse(txt);
    } catch (e) {
        console.error(
            e, 
            "\nAraised from:", txt, 
            "\nRemote:", `${info.address}:${info.port}`
        );
        return;
    }

    printSrv(srvInfo, info.address);
    if (!srvs.filter(e => e.ip == info.address).length) {
        srvInfo.ip = info.address;
        srvs.push(srvInfo);
    }

});

server.listen(selfHTTPPort)

search();