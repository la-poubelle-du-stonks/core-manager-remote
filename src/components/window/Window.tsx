import "./window.scss"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconDefinition, faClose, faWind } from "@fortawesome/free-solid-svg-icons";

type WindowProps = {
    fixedRatio: boolean, 
    child?: JSX.Element,
    title: string,
    icon?: IconDefinition,
}

function acrylicWindow (props: WindowProps) {
    return (
        <div className={`window ${props.fixedRatio ? "" : "alt"}`}>

            <div className="window-titlebar">
                {
                    props.icon 
                        ? <FontAwesomeIcon icon={props.icon}></FontAwesomeIcon> 
                        : undefined
                }                
                <span className="title">{props.title}</span>
                <span className="close">
                    <FontAwesomeIcon icon={faClose}></FontAwesomeIcon>
                </span>
            </div>

            <div className="window-content">
                {props.child}
            </div>

        </div>
    );
}

function aeroWindow (props: WindowProps) {
    return (
        <div className={`window ${props.fixedRatio ? "" : "alt"}`}>

            <div className="bg"></div>
            <div className="outline"></div>

            <div className="window-titlebar">
                {
                    props.icon 
                        ? <FontAwesomeIcon icon={props.icon}></FontAwesomeIcon> 
                        : undefined
                } 
                <span className="title">{props.title}</span>
                <span className="close">
                    <span className="i">
                        <FontAwesomeIcon icon={faClose}></FontAwesomeIcon>
                    </span>
                </span>
            </div>

            <div className="window-content cont">
                {props.child}
            </div>

        </div>
    );
}

function buildWindow (
    theme: 'acrylic' | 'aero',
    props: WindowProps
) {
    switch (theme) {
        case "acrylic":
            return acrylicWindow(props);

        case 'aero':
            return aeroWindow(props);
    }
}

export function SnappyWindow (props: {
    children?: JSX.Element,
    fixedRatio?: boolean,
    theme?: 'acrylic' | 'aero',
}) {

    const windowProps: WindowProps = {
        child: props?.children,
        fixedRatio: (props.fixedRatio != undefined) ? props.fixedRatio : true,
        title: "Fenêtre",
        icon: faWind
    }


    const window = buildWindow(
        props.theme || 'acrylic', 
        windowProps,
    );

    return windowProps.fixedRatio ? (
        <div className="outerWin">
            <div className="windowCont">
              { window }  
            </div>
        </div>
    ) : window;

}