
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { 
    faTableCellsLarge,
    faLayerGroup,
    faList
} from "@fortawesome/free-solid-svg-icons"

export function LayoutButton (props: {
    layoutType: number,
    clickHandle: () => void,
    rightClickHandle: () => void,
}) {

    const icons = [ faTableCellsLarge, faList, faLayerGroup ];
    let layoutType = props.layoutType % 3;

    return (
        <button 
            className="root-layout-btn medium transparent" 
            onClick={() => {
                props.clickHandle();
            }}
            onContextMenu={e => {
                e.preventDefault();
                props.rightClickHandle();
            }}
        >
            <FontAwesomeIcon icon={icons[layoutType]}></FontAwesomeIcon>
        </button>
    )

}