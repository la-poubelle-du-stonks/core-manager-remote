import "./navbar.scss";
import {LayoutButton} from "./LayoutButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCompress, faExpand } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";

export function Navbar (props: {
    layoutType: number,
    layoutChange: (backwards?: boolean) => void,
    children: JSX.Element | JSX.Element[]
}) {

    const children = ("length" in props.children) 
                 ? props.children 
                 : [ props.children ];

    const [ fullscreen, setFullscreen ] = useState(false);

    let fullscreenDetector = () => {
        setFullscreen(!!document.fullscreenElement);
    }

    useEffect (() => {
        document.addEventListener("fullscreenchange", fullscreenDetector);
        return () => document.removeEventListener(
                        "fullscreenchange", 
                        fullscreenDetector
                    );
    })

    return (
        <div 
            className="root-navbar"
            onContextMenu={e => e.preventDefault()}
        >
            <LayoutButton 
                layoutType={props.layoutType}
                clickHandle={() => props.layoutChange()}    
                rightClickHandle={() => props.layoutChange(true)}    
            ></LayoutButton>

            <button 
                className="medium transparent"
                onClick={() => {
                    if (!fullscreen) {
                        document.body.requestFullscreen();
                    } else {
                        document.exitFullscreen();
                    }
                }}
            >
                <FontAwesomeIcon 
                    icon={fullscreen ? faCompress : faExpand}
                ></FontAwesomeIcon>
            </button>

            {...children}


            <span className="bg"></span>
        </div>
    )

}