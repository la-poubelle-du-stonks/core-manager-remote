import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";

export function PageSelector (props: {
    pageCount: number,
    page: number,
    next: () => void,
    prev: () => void,
}) {

    let { pageCount, page, next, prev } = props;
    let style: React.CSSProperties = {};

    if (pageCount == 1) style.display = "none";

    return (
        <div 
            className="root-layout-page-selector"
            style={style}>            
            <button 
                onClick={prev}
                className="transparent medium"
            >
                <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>
            </button>
            <span 
                style={{ 
                fontFamily: "monospace",  
                fontSize: 12,
                whiteSpace: "pre"
                }}
            >{page}/{pageCount}</span>
            <button 
                onClick={next}
                className="transparent medium"
            >
                <FontAwesomeIcon icon={faArrowRight}></FontAwesomeIcon>
            </button>
        </div>
    )

}