import { useEffect, useState } from "react";
import "./layoutManager.scss";

const PANEL_DIVISION = [ "single", "bi", "tri", "quad", "list", "bi stack" ]

export function LayoutManager (props: {
    children: JSX.Element | JSX.Element[],
    pages: { page: number, pageCount: number }
    layout: number,
    fixedRatio: boolean
}) {

    const children = props.children ? (("length" in props.children) 
                 ? props.children 
                 : [ props.children ]) : [];

    const [ size, setSize ] = useState({
    	w: window.innerWidth,
	h: window.innerHeight - 48
    });

    const windowResizeListener = () => setSize({
    	w: window.innerWidth,
	    h: window.innerHeight - 48
    });

    let panels = [];
    for (let i = 0; i < children.length; i += 4) {
        let childs = [];
        for (let j = i; j - i < 4 && j < children.length; j++) {
            childs.push(children[j]);
        }
        let classList = [ "layoutPanel" ];

        let p = ~~(i / 4) + 1;
        if (props.pages.page > p) classList.push("past");
        if (props.pages.page < p) classList.push("future");

        if (size.w < 860) {
            classList.push(PANEL_DIVISION[4]);
        } else if (childs.length == 3 && (size.w / 3 < 400 || props.fixedRatio)) {
            classList.push(PANEL_DIVISION[3]);
        } else if (childs.length == 2 && (size.w < size.h)) {
	    classList.push(PANEL_DIVISION[5]);
	} else {
            classList.push(PANEL_DIVISION[childs.length - 1]);
        }

        // console.log(width)

        let panel = <div className={classList.join(" ")} >{childs}</div>;
        panels.push(panel);
    }

    useEffect(() => {

        window.addEventListener("resize", windowResizeListener);
	screen.orientation.addEventListener("change", windowResizeListener);

        return () => {
            window.removeEventListener("resize", windowResizeListener);
	    screen.orientation.removeEventListener("change", windowResizeListener);
        }

    })

    return (
        <div className="layoutManager">
            
            {/* TEMP */}
            {panels}

	    {JSON.stringify(size)}

        </div>
    )

}
