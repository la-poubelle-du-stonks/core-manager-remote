import { LogoPolybot } from "./assets/logo"

import { Navbar } from "./components/navbar/Navbar";
import { LayoutManager } from "./components/LayoutManager";
import { PageSelector } from "./components/navbar/PageSelector";
import { SnappyWindow } from "./components/window/Window";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus, faImage, faAnchor, faArrowsLeftRight, faLowVision, faVirusCovid } from "@fortawesome/free-solid-svg-icons";

import { NiceButton } from "./components/navbar/NiceButton"

import "./app.scss";
import { useState } from "react";

function updateBodyElement (
  theme: "acrylic" | "aero", 
  solid: boolean
) {

  // Theme
  document.body.classList.toggle("acrylic-theme", theme == "acrylic");
  document.body.classList.toggle("aero-theme", theme == "aero");

  // Solid
  document.body.classList.toggle("solid", solid);
}

export function App () {

  const [ theme, setTheme ] = useState<'acrylic' | 'aero'>("acrylic");
  const [ solid, setSolid ] = useState(false);

  const [ layoutType, setLayoutType ] = useState(0);
  const [ page, setPage ] = useState(1);
  const [ winCount, setWinCount ] = useState(1);
  const [ fixedRatio, setFixedRatio ] = useState(true);
    
  let btns: (ReturnType<typeof NiceButton>)[] = [];
  for (let i = 0; i < winCount; i++) {
    btns.push(NiceButton({id: i}));
  }
  let btnFrag = <>{...btns}</>

  let wins: JSX.Element[] = [];
  for (let i = 0; i < winCount; i++) {
    wins.push(<SnappyWindow fixedRatio={fixedRatio} theme={theme}>
        <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo beatae nulla ut voluptas deserunt explicabo totam. Pariatur unde error similique. Pariatur aliquam eligendi odio reiciendis earum illum at soluta dolores.</span>
      </SnappyWindow>);
  }

  const pageCount = winCount > 0 ? ~~((winCount - 1) / 4) + 1 : 1;

  updateBodyElement(theme, solid);

  return (<>
    
    <div className="bgLogo">
      <LogoPolybot 
        background="var(--bg)" 
        mainColor="var(--fg-neutre)" 
      ></LogoPolybot>
    </div>

    <LayoutManager
      pages={{ page, pageCount }}
      layout={layoutType}
      fixedRatio={fixedRatio}
    >
      {...wins}
    </LayoutManager>

    <Navbar 
      layoutType={layoutType}
      layoutChange={b => setLayoutType(n => (n + (b ? 2 : 1)) % 3)}
    >
      
      <button onClick={() => {
          const p = Math.max(0, winCount - 1);
          setWinCount(p);
          setPage(n => Math.max(1, Math.min(~~((p - 1) / 4) + 1, n)));
        }} className="transparent medium">
        <FontAwesomeIcon icon={faMinus}></FontAwesomeIcon>
      </button>
      
      <button 
          onClick={() => setWinCount(n => n + 1)} 
          className="transparent medium"
      >
        <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
      </button>

      <button 
        className="primary medium transparent"
        onClick={() => { 
          document.body.style.setProperty(
            "--image", 
            "url(/bg.jpg)"
          );
        }}
      >
        <FontAwesomeIcon icon={faImage}></FontAwesomeIcon>
        <span>Image</span>
      </button>

      <button 
        className="medium transparent"
        onClick={() => setFixedRatio(!fixedRatio)}
      >
        <FontAwesomeIcon icon={ fixedRatio ? faAnchor : faArrowsLeftRight }></FontAwesomeIcon>
        <span>Type Fenêtre</span>
      </button>

      <button 
        className="medium transparent"
        onClick={() => setSolid(t => !t)}
      >
        <FontAwesomeIcon icon={faLowVision} ></FontAwesomeIcon>
      </button>

      <button 
        className="medium transparent"
        onClick={() => setTheme(t => {
          let a: ('acrylic' | 'aero')[] = ["acrylic", "aero"];
          return a[(a.indexOf(t) + 1) % 2];
        })}
      >
        <FontAwesomeIcon icon={faVirusCovid} ></FontAwesomeIcon>
      </button>

      <PageSelector
        page={page}
        pageCount={pageCount}
        prev={() => setPage(n => Math.max(1, n - 1))}
        next={() => setPage(n => Math.min(pageCount, n + 1))}
      ></PageSelector>

      {/*btnFrag*/}

    </Navbar>
    
  </>)

}
